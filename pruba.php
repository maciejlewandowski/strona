﻿<!DOCTYPE HTML>

<html lang="pl">

<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Parafia Głożyny</title>
	<meta name="description" content="Strona parafii rzymskokatolickiej Niepokalanego Serca Maryi w Radlinie-Głożynach!">
	<meta name="keywords" content="Radlin, Głożyny, parafia, sakramenty, pogrzeb, chrzest, intencje mszalne, ogłoszenia">
	<link rel="shortcut icon" href="./img/favicon.ico" />
	<meta name="author" content="Maciej Lewandowski">
	<link rel="Stylesheet" type="text/css" href="./css/main.css" />
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	<script src="./js/script.js" type="text/javascript"></script>
	<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
	<script>tinymce.init({ selector:'textarea' });</script>
</head>

<body>

<header>

<section>
	<div style="weidth: 100%; height: 72px"></div>
	<!--<nav>
	<div class="nav">
		<button id="menu-button">MENU</button>
		<ol id="menu">

			<li>
				<a href="#">Ogłoszenia</a>
				<ul>
					<li><a href="#">Ogłoszenia parafialne</a></li>
					<li><a href="#">Intencje mszalne</a></li>
					<li><a href="#">Porządek nabożeństw</a></li>
				</ul>
			</li>

			<li>
				<a href=#>Sakramenty i sakramentalia</a>
				<ul>
					<li><a href="#">Chrzest</a></li>
					<li><a href="#">Bierzmowanie</a></li>
					<li><a href="#">Eucharystia (Wczesna i I Komunia)</a></li>
					<li><a href="#">Spowiedź</a></li>
					<li><a href="#">Sakrament chorych</a></li>
					<li><a href="#">Małżeństwo</a></li>
					<li><a href="#">Święcenia</a></li>
					<li><a href="#">Pogrzeb</a></li>
				</ul>
			</li>

			<li>
				<a href=#>O parafii</a>
			</li>

			<li style="width: 250px;"> 
				<a href="http://www.glozyny.katowice.opoka.org.pl/pruba.php"><img src="./img/logo_300x300.png" style="width: 200px; height: 200px; margin: -75px;"></a>
			</li>

			<li>
				<a href=#>Grupy parafialne</a>
			</li>

			<li>
				<a href=#>Cmentarz</a>
			</li>

			<li>
				<a href=#>Kancelaria</a>
			</li>

		</ol>
	</div>
	</nav> --!>

<?php
include './php/menu/menu.php';
	$sciezka = './php/menu/zmienne.php';
	echo menu($sciezka);
?>

</section>

</header>

<!-- <textarea>Next, get a free Tiny Cloud API key!</textarea> --!>


<div id="container">
	<iframe src='
	<?php
		include './php/ogloszenia.php';
		$ogloszenia = new OgloszeniaIntencje;
		echo $ogloszenia->sciezkaDoPliku($ogloszenia->dataOgloszenia());
	?>
	'> </iframe>
	
</div>


<header></header>
<nav></nav>
<article></article>
<section></section>
<main></main>
<aside></aside>
<footer>
	<div class="footer">
		<p> &copy; <?php echo date('Y'); ?> Rzymskokatolicka parafia NSM w Radlinie<p>
	</div>
</footer>



<?php
/*

strtotime()

int strtotime ( string $czas [, int $teraz ] )



Parsuje większość angielskich tekstowych opisów daty i czasu do uniksowego znacznika czasu.

Funkcja przyjmuje tekst zawierający datę w formacie angielskim i stara się przeliczyć ją na uniksowy znacznik czasu, relatywnie do znacznika czasu podanego w teraz , lub aktualnego czasu, jeśli znacznik nie zostanie podany. W przypadku fiaska, zwracane jest -1.

Informacja: Poprawny zakres znacznika czasu to zwykle od piątku, 13 grudnia 1901 20:45:54 GMT (czasu Greenwich) do wtorku, 19 stycznia 2038 03:14:07 GMT. (Wartości te odpowiadają minimalnej i maksymalnej wartości 32-bitowej liczbie całkowitej ze znakiem).



*/